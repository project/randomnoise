By adding as much noise as possible to your users' traffic, this module aims to make selling your users' data as worthless as possible to potential buyers.

Enabling this module will cause your web site visitors to make a single request to a random IP address, for every request that you serve with the script tag, in order to add noise to the logs being kept by ISPs.
